from django.conf.urls import patterns, url

from models import Post

urlpatterns = patterns('',
    url(r'^post/(?P<slug>[-\w]+)$', 'blog.views.view_post', name='blog_post_detail'),
    url(r'^add/post$', 'blog.views.add_post', name='blog_add_post'),
)